package id.ac.ui.cs.advprog.itemmanagementservice.repository;

import id.ac.ui.cs.advprog.itemmanagementservice.model.SubscriptionBox;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubscriptionBoxRepository extends JpaRepository<SubscriptionBox, Long> {
}
