package id.ac.ui.cs.advprog.itemmanagementservice.controller;

import id.ac.ui.cs.advprog.itemmanagementservice.model.SubscriptionBox;
import id.ac.ui.cs.advprog.itemmanagementservice.service.BoxItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

@RestController
@RequestMapping("/api/box")
public class SubscriptionBoxController {
    @Autowired
    private BoxItemService BoxItemService;

    @GetMapping("/all")
    public ResponseEntity<List<SubscriptionBox>> getAllSubscriptionBoxes() {
        List<SubscriptionBox> boxes = BoxItemService.getAllBoxes();
        return ResponseEntity.ok(boxes);
    }


}