package id.ac.ui.cs.advprog.itemmanagementservice.service;

import id.ac.ui.cs.advprog.itemmanagementservice.model.SubscriptionBox;
import id.ac.ui.cs.advprog.itemmanagementservice.repository.SubscriptionBoxRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BoxItemService {
    @Autowired
    private SubscriptionBoxRepository boxRepo;
    public List<SubscriptionBox> getAllBoxes() {
        return boxRepo.findAll();
    }
}
